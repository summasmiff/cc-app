import React from "react";
import "./App.css";

function App() {
  return (
    <div className="App">
      <h1>Please enter a valid credit card number.</h1>
      <CCForm />
    </div>
  );
}

class CCForm extends React.Component {
  /*
    A credit card is valid if the following rules apply:
    1. Must be 16 digits long.
    2. From the last (rightmost) digit of your card number, double every other digit.
    3. If the doubled digit is larger than 9 (ex. 8 * 2 = 16), subtract 9 from the product (16–9 = 7).
    4. Sum the digits.
    If there is no remainder after dividing by 10 (sum % 10 == 0), the card is valid.
  */

  constructor(props) {
    super(props);
    this.state = {
      is_valid: false,
      value: ""
    };

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  render() {
    return (
      <div>
        <input
          value={this.state.value}
          onChange={this.handleChange}
          className="card-number"
        />
        <button disabled={!this.state.is_valid}>
          <h3>Submit</h3>
        </button>
      </div>
    );
  }
}

export default App;
